package kz.aitu.Midterm.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "person")
public class Person {
@Id
   private long id;
   private String firstname;
   private String lastname;
   private String city;
   private long   phone;
   private long telegram;
}
