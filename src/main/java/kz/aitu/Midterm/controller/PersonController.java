package kz.aitu.Midterm.controller;

import kz.aitu.Midterm.model.Person;
import kz.aitu.Midterm.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonController {
    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/api/v2/users/")
    public ResponseEntity<?> getPerson(){
        return personService.getPerson();
    }

    @GetMapping("/api/v2/users/{id}")
    public ResponseEntity<?> findPersonByID(@PathVariable long id){
        return personService.findPersonByID(id);
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void deletePersonByID(@PathVariable long id){
        personService.deletePersonByID(id);
    }

    @RequestMapping(value = "/api/v2/users/{id}/{eventType}", method = RequestMethod.GET)
    public void updatePersonByID(@PathVariable("id") long id, @PathVariable("eventType") String eventType){
        personService.updatePersonByID(id,eventType);
    }

    @PostMapping("/api/v2/users/")
    public ResponseEntity<?> createPerson(@RequestBody Person person){
        return ResponseEntity.ok(personService.create(person));
    }
}
