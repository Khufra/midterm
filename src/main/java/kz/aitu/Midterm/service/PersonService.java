package kz.aitu.Midterm.service;


import kz.aitu.Midterm.model.Person;
import kz.aitu.Midterm.repository.PersonRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
    private final PersonRepository personRepository;
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public ResponseEntity<?> getPerson(){
        return ResponseEntity.ok(personRepository.findAll());
    }

    public ResponseEntity<?> findPersonByID(long id){
        return ResponseEntity.ok(personRepository.findById(id));
    }

    public void deletePersonByID(long id){
        personRepository.deleteById(id);
    }

    public void updatePersonByID(long id, String eventType){
        personRepository.updateEventTypeByID(eventType, id);
    }

    public Person create(Person person){
        return personRepository.save(person);
    }
}
